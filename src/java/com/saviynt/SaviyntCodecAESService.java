package com.saviynt;


import com.saviynt.encrypt.EncryptionService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SaviyntCodecAESService {

    public static String getSavComps(){

        List filteredStackTraceList = getCallHierarchyList();
        boolean isvalidcall = checkvalidCallHierarchy(filteredStackTraceList, ALLOWED_CALLS_TO_GET_SAVCOMP_SET);

        String retval;

        if(isvalidcall) {
            retval= EncryptionService.getSavComps();
        } else {
            retval = null;
        }
        return  retval;
    }

    public static void setCBCEnabled(String cbcEnabledValue){
        EncryptionService.setCBCEnabled(cbcEnabledValue);
    }

    public static String decode(String strToDecrypt, String oldVal) {

        List filteredStackTraceList = getCallHierarchyList();
        boolean isvalidcall = checkvalidCallHierarchy(filteredStackTraceList, ALLOWED_CALLS_TO_DECODE_WITH_OLDKEY_SET);

        String retval;

        if(isvalidcall) {
            retval = EncryptionService.decode(strToDecrypt, oldVal);
        } else {
            retval = null;
        }
        return retval;
    }

    public static String decode(String strToDecrypt) {

        List filteredStackTraceList = getCallHierarchyList();
        boolean isvalidcall = checkvalidCallHierarchy(filteredStackTraceList, ALLOWED_CALLS_TO_DECODE_SET);
        String retval;

        if(isvalidcall) {
            retval =  EncryptionService.decode(strToDecrypt);
        } else {
            retval = null;
        }
        return retval;
    }

    public static String encode(String strToEncrypt){
        return EncryptionService.encode(strToEncrypt);
    }


    private static List getCallHierarchyList(){
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();

        List filteredStackTraceList = new ArrayList();

        int finalindex = stackTraceElements.length;
        if(stackTraceElements.length > 10) {
            finalindex = 10;
        }

        for(int i = 1 ; i < finalindex ; i++){
            String className = stackTraceElements[i].getClassName();
            String filename = stackTraceElements[i].getFileName();
            if(filename == null && className.contains("com.saviynt.SaviyntCodecAESService$")
                    || "CallSiteArray.java".equalsIgnoreCase(filename) ||
                    "SaviyntCodecAESService.java".equalsIgnoreCase(filename) ||
                    "AbstractCallSite.JAVA".equalsIgnoreCase(filename) ){
                // Ignore entries
            } else {
                filteredStackTraceList.add(stackTraceElements[i]);
            }
        }
        return filteredStackTraceList;
    }

    private static boolean checkvalidCallHierarchy(List callHierarchList , Set allowset) {
        boolean retval = false ;

        try {
            if (callHierarchList != null && callHierarchList.size() > 0) {
                StackTraceElement stackTraceElement = (StackTraceElement) callHierarchList.get(0);

                String className = stackTraceElement.getClassName();
                String filename = stackTraceElement.getFileName();

                if (allowset.contains(filename)) {
                    retval = true;
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return retval;
    }

    private static HashSet ALLOWED_CALLS_TO_GET_SAVCOMP_SET = new HashSet();
    private static HashSet ALLOWED_CALLS_TO_DECODE_WITH_OLDKEY_SET = new HashSet();

    private static HashSet ALLOWED_CALLS_TO_DECODE_SET = new HashSet();

    static {
        try {
            ALLOWED_CALLS_TO_DECODE_SET.add("ExternalPingJob.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("IncomingMailARSJob.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("ConnectorMsHelperService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("ReceiveMailService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("ElasticSearchService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("Users.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("ArsTaskService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("EmailerService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("SaviyntCommonUtilityService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("SaviyntElasticService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("WorkflowmanagementService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("WorkflowService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("UserChangeActionService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("UserChangeActionv2Service.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("ExternalConnectorService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("ArsTasks.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("EcmConfigController.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("ExternalconnectionAttributeValue.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("WorkflowmanagementController.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("RacfLdapProvisioningService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("EmailHistoryService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("MicroserviceApiCallService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("PamService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("PasswordManagementService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("BoxProvisioningService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("RestProvisioningService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("VaultsSystemService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("RestTokenGeneratorService.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("JBPMMailSession.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("DataSourceGrailsPlugin.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("ParentAbstractEcmConfigController.groovy");
            ALLOWED_CALLS_TO_DECODE_SET.add("AbstractSuperWorkflowmanagementController.groovy");


            ALLOWED_CALLS_TO_GET_SAVCOMP_SET.add("SaviyntCommonUtilityService.groovy");
            ALLOWED_CALLS_TO_GET_SAVCOMP_SET.add("HomeController.groovy");
            ALLOWED_CALLS_TO_GET_SAVCOMP_SET.add("DataSourceGrailsPlugin.groovy");

            ALLOWED_CALLS_TO_DECODE_WITH_OLDKEY_SET.add("SaviyntCommonUtilityService.groovy");


        } catch (Exception var1) {
            var1.printStackTrace();
        }
    }



    /*public static void main(String args[]) {
        String sss=SaviyntCodecAESService.encode("password");
        System.out.println(sss);
        System.out.println(SaviyntCodecAESService.decode(sss));

    }*/


/*    private static byte[] encryptCBC(String plainText, String savSet) throws Exception {
        byte[] clean = plainText.getBytes();

        // Generating IV.
        int ivSize = 16;
        byte[] iv = new byte[ivSize];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        // Hashing key.
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(savSet.getBytes("UTF-8"));
        byte[] keyBytes = new byte[16];
        System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Encrypt.
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(clean);

        // Combine IV and encrypted part.
        byte[] encryptedIVAndText = new byte[ivSize + encrypted.length];
        System.arraycopy(iv, 0, encryptedIVAndText, 0, ivSize);
        System.arraycopy(encrypted, 0, encryptedIVAndText, ivSize, encrypted.length);

        return encryptedIVAndText;
    }

    private static String decryptCBC(byte[] encryptedIvTextBytes, String savSet) throws Exception {
        int ivSize = 16;
        int keySize = 16;

        // Extract IV.
        byte[] iv = new byte[ivSize];
        System.arraycopy(encryptedIvTextBytes, 0, iv, 0, iv.length);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        // Extract encrypted part.
        int encryptedSize = encryptedIvTextBytes.length - ivSize;
        byte[] encryptedBytes = new byte[encryptedSize];
        System.arraycopy(encryptedIvTextBytes, ivSize, encryptedBytes, 0, encryptedSize);

        // Hash key.
        byte[] keyBytes = new byte[keySize];
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(savSet.getBytes());
        System.arraycopy(md.digest(), 0, keyBytes, 0, keyBytes.length);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Decrypt.
        Cipher cipherDecrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] decrypted = cipherDecrypt.doFinal(encryptedBytes);

        return new String(decrypted);
    }

    private static String getProp(String propName, String fileName){
        String propValue = null;
        try {
            if (System.getenv("SAVIYNT_HOME") !=null) {
                String home = System.getenv("SAVIYNT_HOME");
                String extfilepath = home + "/" + fileName;
                InputStream is = new FileInputStream(extfilepath);
                if( is !=null ) {
                    prop = new Properties();
                    prop.load(is);
                    propValue = prop.getProperty(propName);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return propValue;
    }

    public static String getSavComp1(){
        return "S@v!yn";
    }*/

}

