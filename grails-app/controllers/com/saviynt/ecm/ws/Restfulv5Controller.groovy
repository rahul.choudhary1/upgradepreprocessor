package com.saviynt.ecm.ws

import com.saviynt.ecm.utility.domain.EcmConfig
import grails.converters.JSON

class Restfulv5Controller {
    static allowedMethods = [
            getTriggersExportStatus               : 'GET'
    ]
    def getTriggersExportStatus() {
        Map result = new HashMap()
        int errorCode = 0
        String msg = "WIP"
        try {
            EcmConfig config = EcmConfig.findById("G2_TRIGGERS_EXPORT_STATUS")
            if (config != null && config.configData!=null && config.configData!="") {
                msg = config.configData.trim().equalsIgnoreCase("true")?"Success":"Error"
                errorCode = config.configData.trim().equalsIgnoreCase("true") ? 0 : 1
            }
            result.put("errorCode", errorCode)
            result.put("msg", msg)
            response.status = 200
        } catch (Exception e) {
            log.error("Error in API", e)
            result.put("errorCode", "1")
            result.put("msg", "Unexpected error occurred - ${e.getMessage()}")
            response.status = 500
        }
        render(contentType: "text/json", text: result as JSON)
    }
}
