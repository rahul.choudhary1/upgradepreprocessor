package com.saviynt.ecm.utility.domain

class EcmConfig implements Serializable {
    private static final long serialVersionUID = 115671L;
    /*SD-5304*/
    String category
    String id
    String configData
    byte[] icon
    String configGroup
    String configSubGroup
    String objectLabel
    String description
    Integer objectType
    Date updateDate
    Integer configOrder
    Integer configGroupOrder
    Integer configSubGroupOrder
    String possibleValues
    Boolean isDeprecated = false
    String childKeys
    String disableElement
    Boolean helpIconEnable= false   /*SD-15248*/


    public static final int TEXT_WITH_SAVE = 1;
    public static  final int CHECKBOX = 2;
    public static  final int SINGLE_SELECT = 3;
    public static  final int MULTI_SELECT = 4;
    public static  final int DATA_TABLE = 5;
    public static  final int RULE_BUTTON = 6;
    public static  final int TEXT_AREA = 7;
    public static  final int NUMBER = 8;
    public static  final int HIDDEN = 9;
    public static  final int SINGLE_SELECT_CONFIRM_POPUP = 10; /*SD-16361*/

    public static final Map<Integer, String> objectTypeMap = new HashMap<>()

    static {
        objectTypeMap.put(TEXT_WITH_SAVE, "textwithsave")
        objectTypeMap.put(CHECKBOX, "checkbox")
        objectTypeMap.put(SINGLE_SELECT, "single-select")
        objectTypeMap.put(MULTI_SELECT, "multi-select")
        objectTypeMap.put(DATA_TABLE, "data-table")
        objectTypeMap.put(RULE_BUTTON, "rule-button")
        objectTypeMap.put(TEXT_AREA, "textarea")
        objectTypeMap.put(NUMBER, "number")
        objectTypeMap.put(HIDDEN, "hidden")
        objectTypeMap.put(SINGLE_SELECT_CONFIRM_POPUP, "single-select_confirm_popup") /*SD-16361*/


    }

    static constraints = {
        category(nullable:true)
        icon(nullable:true)
        configGroup(nullable:true)
        configSubGroup(nullable:true)
        updateDate(nullable:true)
        objectLabel(nullable:true)
        objectType(nullable:true)
        configOrder(nullable: true)
        configGroupOrder(nullable: true)
        configSubGroupOrder(nullable: true)
        possibleValues(nullable: true)
        childKeys(nullable: true)
        description(nullable: true)
        isDeprecated(nullable: true)
        disableElement(nullable: true)
        helpIconEnable(nullable: true)  /*SD-15248*/

    }
    static mapping = {
        table 'CONFIGURATION'
        id column: 'NAME', generator: 'assigned'
        version false
        columns {
            category column: 'CATEGORY',sqlType: 'text'
            configData column: 'CONFIGDATA', type: 'text'
            icon column: 'ICON', sqlType: 'blob'
            configGroup column: 'CONFIG_GROUP', sqlType: 'text'
            configSubGroup column: 'CONFIG_SUB_GROUP', sqlType: 'text'
            objectLabel column: 'OBJECT_LABEL'
            objectType column: 'OBJECT_TYPE'
            updateDate column: 'UPDATE_DATE'
            configOrder column: 'CONFIG_ORDER'
            configGroupOrder column: 'CONFIG_GROUP_ORDER'
            configSubGroupOrder column: 'CONFIG_SUB_GROUP_ORDER'
            possibleValues column: 'POSSIBLE_VALUES', type: 'text'
            description  column: 'DESCRIPTION', sqlType: 'text'
            isDeprecated column: 'DEPRECATED'
            childKeys column: 'CHILD_KEYS', sqlType: 'text'
            disableElement column: 'DISABLE_ELEMENT'
            helpIconEnable column: 'HELPICONENABLE'   /*SD-15248*/


        }
    }
}
