package com.saviynt.utility

import com.saviynt.ecm.utility.domain.EcmConfig
import grails.util.Holders
import groovy.sql.Sql
import org.json.simple.JSONObject
import org.quartz.JobDataMap;

class MigrationService {

    def quartzScheduler
    def dataSource
    def serviceMethod() {}

     public Map exportTriggers(Map params){
        log.debug("Enter exportTriggers ")
        Map returnMap = new HashMap()
        try{
            StringBuilder stringBuilder = new StringBuilder()
            stringBuilder.append(System.getenv("SHARED_BASE_FOLDER")).append("/${Holders.config.triggers.exportPath}")
            def fileExportpath=stringBuilder.toString()
            if (!(new File(fileExportpath).exists())) {
                log.debug("Directory does not exist, creating new directory: " + fileExportpath)
                new File(fileExportpath).mkdirs()
            }
            File triggers = new File(fileExportpath + File.separator + "triggers.json")
            log.debug "Triggers File created " + triggers.getAbsolutePath()
            JSONObject inputJSON = new JSONObject()
            String triggersQuery = "Select * from qrtz_triggers where NEXT_FIRE_TIME != -1"
            List triggersFromDB
            def db = new Sql(dataSource)
            triggersFromDB = db.rows(triggersQuery)
            if(triggersFromDB?.size()>0) {
                List jobGroupNameListfromG5 = ["utility", "Analytics","DATABASE","DATA","Attestation","BaseLine","SOD","SAP","ecmGroup","EPIC","CONNECTOR","ECFConnector","Schema","IdentityMatchAndMerge","System" ,"oia","RACF","REPORTS","ControlCenter"]
                List triggersListToImport = new ArrayList()
                triggersFromDB.each {
                    def jobDataMap = processJobDetail(new ByteArrayInputStream(it.JOB_DATA), null)
                    if(jobDataMap!=null) {
                        it.putAll(jobDataMap)
                    }
                    it.remove("JOB_DATA")
                    if(it.containsKey('jobtriggername')) {
                        it.put("jobtriggername", it.jobtriggername?.toString())
                    }
                    if(it.containsKey('IS_VOLATILE')){
                        it.remove('IS_VOLATILE')
                    }
                    String JOB_GROUP = it.JOB_GROUP
                    jobGroupNameListfromG5.each {
                        if(it.equalsIgnoreCase(JOB_GROUP)){
                            JOB_GROUP = it
                        }
                    }
                    it.JOB_GROUP = JOB_GROUP
                    triggersListToImport.add(it)
                }
                inputJSON.put("Triggers", triggersListToImport)
            }
            triggers.write(inputJSON.toString())
            log.debug("Triggers exported successfully..")
            /*log.debug("deleting existing triggers")
            triggersFromDB.each {
                try {
                    boolean response = quartzScheduler.unscheduleJob(it.TRIGGER_NAME, it.TRIGGER_GROUP)
                    log.debug("delete response for ${it.TRIGGER_NAME} - ${response}")
                } catch (Exception e) {
                    log.error("Exception occured while deleting job triggers ${it.TRIGGER_NAME} for job ${it.JOB_NAME}", e)
                }
            }*/
            try {
                EcmConfig config = EcmConfig.findById("G2_TRIGGERS_EXPORT_STATUS")
                if (config == null) {
                    config = new EcmConfig()
                    config.id = "G2_TRIGGERS_EXPORT_STATUS"
                }
                config.configData = "true"
                config.save()
            }catch(Exception e){
                log.error("Exception while updating G2_TRIGGERS_EXPORT_STATUS config")
            }
        }catch(Exception e){
            log.error("error occurred while exporting triggers:",e)
            EcmConfig config = EcmConfig.findById("G2_TRIGGERS_EXPORT_STATUS")
            if (config == null) {
                config = new EcmConfig()
                config.id = "G2_TRIGGERS_EXPORT_STATUS"
            }
            config.configData = "false"
            config.save()
        }
        log.debug("Exit exportTriggers ")
        return returnMap
    }
    Map processJobDetail(InputStream is, Map messageMap) throws IOException, ClassNotFoundException {
        Map map = new HashMap()
        if (is.available() == 0)
            return;
        ObjectInputStream ois = new ObjectInputStream(is);
        JobDataMap jobDataMap = (JobDataMap)ois.readObject();
        Iterator keys = jobDataMap.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String)keys.next()
            Object value = jobDataMap.get(key);
            if(messageMap) {
                try{
                    key = messageMap.get(key) ? messageMap.get(key) : key
                } catch (Exception ex) {
                    log.debug("Exception while getting message key for ${key} ")
                    log.debug("EXception ${ex}")
                }
            }
            map.put(key, value)
        }
        return map
    }
}
