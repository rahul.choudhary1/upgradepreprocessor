import com.saviynt.ecm.utility.domain.EcmConfig

class BootStrap {
    def migrationService
    def init = { servletContext ->
        String update = System.getenv("UPGRADE_DB_TO_G5")
        EcmConfig isUpdated = EcmConfig.findById("G2_TRIGGERS_EXPORT_STATUS")
        if("true".equalsIgnoreCase(update) && (isUpdated == null || isUpdated.configData==null || isUpdated.configData=="" || "false".equalsIgnoreCase(isUpdated?.configData)))
            migrationService.exportTriggers([:])
        else
            log.debug("Env Var UPGRADE_DB_TO_G5 should be true and G2_TRIGGERS_EXPORT_STATUS config must be null or blank or false")

    }
    def destroy = {
    }
}
