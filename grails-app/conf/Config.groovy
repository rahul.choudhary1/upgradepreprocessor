System.setProperty("Saviynt.server.starttime", "" + new Date().getTime().toString());

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
        all:           '*/*',
        atom:          'application/atom+xml',
        css:           'text/css',
        csv:           'text/csv',
        form:          'application/x-www-form-urlencoded',
        html:          ['text/html','application/xhtml+xml'],
        js:            'text/javascript',
        json:          ['application/json', 'text/json'],
        multipartForm: 'multipart/form-data',
        rss:           'application/rss+xml',
        text:          'text/plain',
        xml:           ['text/xml', 'application/xml']
]


grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'none' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "html" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

// log4j configuration

/*
log4j = {
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}


    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
            'org.codehaus.groovy.grails.web.pages',          // GSP
            'org.codehaus.groovy.grails.web.sitemesh',       // layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping',        // URL mapping
            'org.codehaus.groovy.grails.commons',            // core / classloading
            'org.codehaus.groovy.grails.plugins',            // plugins
            'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'
    //  'org.springframework.security.web'
    appenders {
        rollingFile name: 'custom', file: 'C:\\Saviynt\\saviynt-software\\saviynt-software\\Conf\\logs\\debugLogNew.log', maxFileSize: '10240KB', maxBackupIndex: 10

    }
    root {
        debug 'custom'
        additivity = true
    }
}
*/



// Added by the Spring Security Core plugin: below should be set to true to enable strict auth
grails.plugin.springsecurity.rejectIfNoRule = false
grails.plugin.springsecurity.fii.rejectPublicInvocations = false

grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.saviynt.ecm.identitywarehouse.domain.Users'
grails.plugin.springsecurity.userLookup.usernamePropertyName = 'username'
grails.plugin.springsecurity.userLookup.enabledPropertyName = 'enabled'
grails.plugin.springsecurity.userLookup.passwordPropertyName = 'password'
grails.plugin.springsecurity.userLookup.authoritiesPropertyName = 'authorities'
grails.plugin.springsecurity.userLookup.accountExpiredPropertyName = 'accountExpired'
grails.plugin.springsecurity.userLookup.accountLockedPropertyName = 'accountLocked'
grails.plugin.springsecurity.userLookup.passwordExpiredPropertyName = 'passwordExpired'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.saviynt.ecm.identitywarehouse.domain.User_savroles'
grails.plugin.springsecurity.authority.className = 'com.saviynt.ecm.identitywarehouse.domain.SavRoles'
grails.plugin.springsecurity.authority.nameField = 'authority'

sav.productname = 'ars'
if (System.getenv('SAVIYNT_HOME')) {
    def home = System.getenv('SAVIYNT_HOME')
    println "Loading File From " + home
    // FON-671 - Added InternalConfig.groovy
    grails.config.locations = ["file:${home}/Config.groovy",
                               "file:${home}/DataSource.groovy",
                               "file:${home}/InternalConfig.groovy",
                               "file:${home}/externalconfig.properties",
                               "file:${home}/AuthenticationConfig.groovy",
                               "file:${home}/TempTables.groovy"
    ]
}
else {
    grails.config.locations = ["file:C:\\Saviynt\\saviynt-software\\saviynt-software\\Conf\\Config.groovy",
                               "file:C:\\Saviynt\\saviynt-software\\saviynt-software\\Conf\\InternalcConfig.groovy",
                               "file:C:\\Saviynt\\saviynt-software\\saviynt-software\\Conf\\externalconfig.properties",
                               "file:C:\\Saviynt\\saviynt-software\\saviynt-software\\Conf\\Datasource.groovy" ,
                               "file:C:\\Saviynt\\saviynt-software\\saviynt-software\\Conf\\AuthenticationConfig.groovy",
                               "file:C:\\Saviynt\\saviynt-software\\saviynt-software\\Conf\\TempTables.groovy"]
    println "SAVIYNT_HOME Environment variable is not defined please define and add config file  "
}



// Uncomment and edit the following lines to start using Grails encoding & escaping improvements

/* remove this line
// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside null
                scriptlet = 'none' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        filteringCodecForContentType {
            //'text/html' = 'html'
        }
    }
}
remove this line */
//grails.plugin.springsecurity.ldap.active=false

//SAML Configuration
/*

grails.plugin.springsecurity.successHandler.alwaysUseDefault=false
grails.plugin.springsecurity.saml.active=true
grails.plugin.springsecurity.saml.alwaysUseAfterLoginUrl=false
grails.plugin.springsecurity.saml.defaultTargetUrl="/"
grails.plugin.springsecurity.saml.afterLoginUrl = '/users/list'
grails.plugin.springsecurity.saml.afterLogoutUrl = '/logout'
grails.plugin.springsecurity.saml.autoCreate.key='username'
grails.plugin.springsecurity.saml.userAttributeMappings=[]
grails.plugin.springsecurity.saml.userGroupToRoleMapping=[:]
grails.plugin.springsecurity.saml.userGroupAttribute=""
grails.plugin.springsecurity.saml.idpSelectionPath="/"
grails.plugin.springsecurity.saml.autoCreate.active = true
grails.plugin.springsecurity.saml.autoCreate.assignAuthorities=true
grails.plugin.springsecurity.saml.metadata.defaultIdp='http://www.okta.com/exk9hb7hjgSfE9wrC0h7'
grails.plugin.springsecurity.saml.metadata.providers = [ping: 'security/idp.xml']
grails.plugin.springsecurity.saml.metadata.sp.file = 'security/sp.xml'
//grails.plugin.springsecurity.saml.metadata.idp.file = '/security/idp.xml'
grails.plugin.springsecurity.saml.metadata.url = '/saml/metadata'
grails.plugin.springsecurity.saml.responseSkew=300
grails.plugin.springsecurity.saml.maxAuthenticationAge=1800
grails.plugin.springsecurity.saml.keyManager.storeFile = 'classpath:security/sp.keystore'
grails.plugin.springsecurity.saml.keyManager.storePass = 'saviynt123'
grails.plugin.springsecurity.saml.keyManager.passwords = [ tomcat: 'saviynt123' ]
grails.plugin.springsecurity.saml.keyManager.defaultKey = 'tomcat'
grails.plugin.springsecurity.saml.metadata.sp.defaults =  [
        local: true,
        alias: 'test',
        securityProfile: 'metaiop',
        signingKey: 'tomcat',
        encryptionKey: 'tomcat',
        tlsKey: 'tomcat',
        requireArtifactResolveSigned: false,
        requireLogoutRequestSigned: false,
        requireLogoutResponseSigned: false,
        idpDiscoveryEnabled: true]
grails.plugin.springsecurity.saml.logouturl='/ECM/saml/logout?local=true'

*/

/*
grails.plugin.springsecurity.rest.token.storage.jwt.secret='Xcaj9dqrD6gh8K6Sq950903sDC3Q06eY6Rqfk21fTErImPYqa4hpQxm0dX'
grails.plugin.springsecurity.rest.token.storage.jwt.expiration=1800
grails.plugin.springsecurity.rest.token.storage.jwt.serviceaccount.expiration = 5184000

// Rest plugin configurations
grails.plugin.springsecurity.rest.active = true
*/

/*
grails.plugin.springsecurity.filterChain.chainMap = [
        '/api/**': 'JOINED_FILTERS,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter',  // Stateless chain
        '/**': 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'                                                                          // Traditional chain
]
*/

// config to add security headers
response.headers = [
        "SET" : [
                "Strict-Transport-Security" :"max-age=31536000; includeSubdomains" ,
                "X-Content-Type-Options" :"nosniff" ,
                "X-XSS-Protection" :"1; mode=block" ,
                //  "Content-Security-Policy" :"default-src 'self'; script-src 'self' https://www.google.com https://www.gstatic.com/ 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; img-src * ; frame-src https:" ,
                "Referrer-Policy": "same-origin",
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Origin" : "http://localhost",
                "Expires" :"Sun, 7 May 1995 12:00:00 GMT" ,
                "Cache-Control" : "no-store, no-cache, must-revalidate" ,
                "Pragma" : "no-cache"
        ] ,
        "ADD" : [
                "Cache-Control" : "post-check=0, pre-check=0"
        ]
]

// Config to set Origin header for CORS
/*cors.headers = [
        'Access-Control-Allow-Origin': 'http://localhost'
]*/

//grails.plugin.springsecurity.useSecurityEventListener = true
/*grails.plugin.springsecurity.onAuthenticationSuccessEvent = { e, appCtx ->
    println "Inside onAuthenticationSuccessEvent"

    Authentication authentication = e.getAuthentication();
    UserDetails userDetails = authentication.getPrincipal();
    def ctx = ApplicationHolder.application.mainContext

}
grails.plugin.springsecurity.saml.fips.active=false*/
// SD-21927 - Uncomment for Microservice architecture.
//grails.plugin.springsecurity.msEnabled = true
