class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        "/api/v5/$action?/$id?"(controller: "restfulv5", parseRequest: true) {
        }
        "/"(view:"/index")
        "500"(view:'/error')
	}
}
