grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

String dependencyCacheEnabled =  System.getenv('DEPENDENCY_CACHE_ENABLED')?.toString()?.trim()
println("dependencyCacheEnabled = ${dependencyCacheEnabled}")

if("TRUE".equalsIgnoreCase(dependencyCacheEnabled)) {
    println("dependencyCacheEnabled is TRUE")
    final String cacheLocation = System.getenv('DEPENDENCY_CACHE_LOCATION')?.toString()?.trim()
    println("cacheLocation = ${cacheLocation}")
    if(cacheLocation !=null && !"".equalsIgnoreCase(cacheLocation?.trim())){
        println("dependency cacheLocation is defined. Setting cache location.")
        grails.dependency.cache.dir = cacheLocation
        println("grails.dependency.cache.dir set to location=${grails.dependency.cache.dir}")
    }
}



grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        mavenLocal()
        grailsCentral()
        mavenCentral()
        mavenRepo "https://download.java.net/maven/2/"
        mavenRepo "https://repository.jboss.com/maven2/"
        mavenRepo "https://repo.grails.org/grails/repo"
        mavenRepo "https://repo.grails.org/grails/core"
        mavenRepo "https://repo.grails.org/grails/plugins/"
        mavenRepo "https://repo.spring.io/milestone/"
        mavenRepo "https://mvnrepository.com/artifact/"
        mavenRepo "https://mvnrepository.com/"
        mavenRepo "https://mvnrepository.com/repos/grails-plugins"
    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', /'runtime', 'test' or 'provided' scopes e.g.
        compile('com.googlecode.json-simple:json-simple:1.1.1')
    }

    plugins {
        // plugins for the build system only
        build ":tomcat:7.0.54"

        compile ":quartz:0.4.2"
        runtime ":hibernate:3.6.10.16" // or ":hibernate4:4.3.5.4"

        runtime ":resources:1.2.8"

    }
}
